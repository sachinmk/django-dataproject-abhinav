from django.core.management.base import BaseCommand
from sqlalchemy import create_engine
import os
import csv
from django.apps import apps
from datetime import datetime
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, String, Date, Float
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Company(Base):
    __tablename__ = 'maharashtradata_maharashtracompanydata'
    corporate_identification_number = Column(String(80), primary_key=True)
    company_name = Column(String(100))
    company_status = Column(String(100))
    company_class = Column(String(100))
    company_category = Column(String(100))
    company_sub_category = Column(String(100))
    date_of_registration = Column(Date)
    registered_state = Column(String(100))
    authorized_cap = Column(Float)
    paidup_capital = Column(Float)
    industrial_class = Column(String(100))
    pba_as_per_cin = Column(String(100))
    registered_office_address = Column(String(200))
    registrar_of_companies = Column(String(100))
    email_addr = Column(String(100))
    latest_year_annual_return = Column(String(100))
    latest_year_financial_statement = Column(String(100))


class Command(BaseCommand):
    help = 'Data will be loaded by this command.'

    def get_current_app_path(self):
        return apps.get_app_config("maharashtradata").path

    def get_csv_file(self, filename):
        app_path = self.get_current_app_path()
        file_path = os.path.join(app_path, "management", "commands", filename)
        return file_path

    def handle(self, *args, **options):
        url = 'postgresql://abhinav_django:postgres@localhost:5432/companies'
        csv_path = self.get_csv_file('csv_data/mca_maharashtra_21042018.csv')

        engine = create_engine(url)
        Session = sessionmaker(bind=engine)
        session = Session()

        with open(csv_path, 'r', encoding="latin_1") as f:
            data = csv.reader(f)
            next(data)
            ls = ['NA', 'Non-govt company', 'Company Limited by Guarantee',
                  'Dissolved', 'Public', '00-01-1900']
            for row in data:
                date = row[6]
                if date in ls:
                    continue
                company_data = Company(
                    corporate_identification_number=row[0],
                    company_name=row[1],
                    company_status=row[2],
                    company_class=row[3],
                    company_category=row[4],
                    company_sub_category=row[5],
                    date_of_registration=datetime.strptime(
                        date, '%d-%m-%Y').strftime('%Y-%m-%d'),
                    registered_state=row[7],
                    authorized_cap=row[8],
                    paidup_capital=row[9],
                    industrial_class=row[10],
                    pba_as_per_cin=row[11],
                    registered_office_address=row[12],
                    registrar_of_companies=row[13],
                    email_addr=row[14],
                    latest_year_annual_return=row[15],
                    latest_year_financial_statement=row[16])
                session.add(company_data)

            session.commit()
            session.close_all()
        self.stdout.write(self.style.SUCCESS('Database updated'))
