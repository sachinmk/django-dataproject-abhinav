# Generated by Django 2.2.15 on 2020-08-28 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maharashtradata', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maharashtracompanydata',
            name='authorized_cap',
            field=models.DecimalField(decimal_places=2, max_digits=13),
        ),
        migrations.AlterField(
            model_name='maharashtracompanydata',
            name='paidup_capital',
            field=models.DecimalField(decimal_places=2, max_digits=13),
        ),
    ]
