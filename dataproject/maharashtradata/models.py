from django.db import models


class MaharashtraCompanyData(models.Model):
    corporate_identification_number = models.CharField(max_length=80)
    company_name = models.CharField(max_length=100)
    company_status = models.CharField(max_length=100)
    company_class = models.CharField(max_length=100)
    company_category = models.CharField(max_length=100)
    company_sub_category = models.CharField(max_length=100)
    date_of_registration = models.DateField()
    registered_state = models.CharField(max_length=100)
    authorized_cap = models.DecimalField(max_digits=18, decimal_places=5)
    paidup_capital = models.DecimalField(max_digits=18, decimal_places=5)
    industrial_class = models.CharField(max_length=100)
    pba_as_per_cin = models.CharField(max_length=100)
    registered_office_address = models.CharField(max_length=200)
    registrar_of_companies = models.CharField(max_length=100)
    email_addr = models.EmailField()
    latest_year_annual_return = models.CharField(max_length=100)
    latest_year_financial_statement = models.CharField(max_length=100)

    def __repr__(self):
        return f"< company_name={self.company_name} >"
