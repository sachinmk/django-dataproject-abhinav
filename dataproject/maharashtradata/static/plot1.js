var input1 = 0;
var input2 = 100000;
var input3 = 2500000;
var input4 = 100000000;
var input5 = 1000000000;
fetchData();


var form = document.getElementById("form1")
form.addEventListener('submit', function (e) {
    e.preventDefault()
    getData();
});

function getData() {
    input2 = document.forms['form1'].elements['input2'].value;
    input3 = document.forms['form1'].elements['input3'].value;
    input4 = document.forms['form1'].elements['input4'].value;
    fetchData();
}

function fetchData() {
    var data = [ input1, input2, input3, input4, input5 ]
    var url = `http://127.0.0.1:8000/authizedcap?data=${data}`
    fetch(url).then(response => response.json().then(function (data) {
        render_chart(data);
    }));
}


function render_chart(data) {
    Highcharts.chart("problem1", {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Company Registration in Maharashtra'
        },
        xAxis: {
            categories: Object.keys(data),
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of Companies'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Authorized Capital',
            data: Object.values(data)

        }]
    });
}
