var start_year = 2000;
var end_year = 2014;
fetchData();

var form = document.getElementById("form1")
form.addEventListener('submit', function (e) {
    e.preventDefault()
    getData();
});

function getData() {
    start_year = document.forms['form1'].elements['input1'].value;
    end_year = document.forms['form1'].elements['input2'].value;
    fetchData();
}

function fetchData() {
    var data = [ start_year, end_year ]
    var url = `http://127.0.0.1:8000/companyregistration?param=${data}`
    fetch(url).then(response => response.json().then(function (data) {
        render_chart(data);
    }));
}


function render_chart(data) {
    Highcharts.chart("problem2", {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Company Registration in Maharashtra'
        },
        xAxis: {
            categories: Object.keys(data),
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of Companies'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Year',
            data: Object.values(data)

        }]
    });
}
