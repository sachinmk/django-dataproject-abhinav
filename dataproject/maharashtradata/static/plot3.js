var year = 2011;

fetchData();


var form = document.getElementById("form1")
form.addEventListener('submit', function (e) {
    e.preventDefault()
    getData()
});

function getData() {
    year = document.forms['form1'].elements['input1'].value;
    fetchData();
}

function fetchData() {
    var data = year
    var url = `http://127.0.0.1:8000/toppba?param=${data}`
    fetch(url).then(response => response.json().then(function (data) {
        render_chart(data);
    }));
}


function render_chart(data) {
    Highcharts.chart("problem3", {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top 10 principle business activity of ' + year
        },
        xAxis: {
            categories: Object.keys(data),
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of Companies'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Principle Business Activity',
            data: Object.values(data)

        }]
    });
}
