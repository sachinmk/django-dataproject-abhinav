var pba_list = [
    "Agriculture and Allied Activities",
    "Business Services",
    "Community, personal & Social Services",
    "Construction",
    "Finance",
    "Insurance",
    "Manufacturing (Food stuffs)",
    "Trading",
    "Transport, storage and Communications",
    "Real Estate and Renting",
];
var selected_pba = [0,1,2,3,4,5,6,7,8,9];

fetchData();


var form = document.getElementById("form1")
form.addEventListener('submit', function (e) {
    e.preventDefault();
    getData();
});

function getData() {
    inputs = ['input1', 'input2', 'input3', 'input4', 'input5',
        'input6', 'input7', 'input8', 'input9', 'input10']
    selected_pba = []
    let count = 0
    for (let input of inputs) {
        if (document.getElementById(input).checked) {
            selected_pba.push(count)
        }
        count++;
    }
    fetchData();
}

function fetchData() {
    var data = selected_pba;
    var url = `http://127.0.0.1:8000/registrationcount?param=${data}`
    fetch(url).then(response => response.json().then(function (data) {
        render_chart(data);
    }));
}


function render_chart(data) {
    let _categories = Object.keys(data)
    let pba_data = Object.keys(data['2009'])
    series_data = []
    for (let cat in pba_data) {
        data_for_series = []
        data_for_series.push(data["2009"][pba_data[cat]])
        data_for_series.push(data['2010'][pba_data[cat]])
        data_for_series.push(data['2011'][pba_data[cat]])
        data_for_series.push(data['2012'][pba_data[cat]])
        data_for_series.push(data['2013'][pba_data[cat]])
        data_for_series.push(data['2014'][pba_data[cat]])
        data_for_series.push(data['2015'][pba_data[cat]])
        data_for_series.push(data['2016'][pba_data[cat]])
        data_for_series.push(data['2017'][pba_data[cat]])
        data_for_series.push(data['2018'][pba_data[cat]])
        cat_data = {
            name: pba_data[cat],
            data: data_for_series
        }
        series_data.push(cat_data)
    }
    Highcharts.chart('problem4', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Registration count over Registration year and given pba'
        },
        xAxis: {
            categories: _categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Registrations (In number)'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: series_data
    });
}
