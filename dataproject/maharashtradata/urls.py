from django.urls import path
from .views import (
    plot1,
    authized_cap,
    plot2,
    company_registration,
    plot3,
    top_10_pba,
    plot4,
    registration_count,
)
urlpatterns = [
    path('', plot1, name='plot1'),
    path('plot2/', plot2, name='plot2'),
    path('plot3/', plot3, name='plot3'),
    path('plot4/', plot4, name='plot4'),
    path('authizedcap/', authized_cap),
    path('companyregistration/', company_registration),
    path('toppba/', top_10_pba),
    path('registrationcount/', registration_count),
]
