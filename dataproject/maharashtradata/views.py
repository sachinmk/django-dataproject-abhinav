from django.shortcuts import render
from django.http import JsonResponse
from django.db.models import Count

from .models import MaharashtraCompanyData


def plot1(request, *args, **kwargs):
    return render(request, 'plot1.html')


def plot2(request, *args, **kwargs):
    return render(request, 'plot2.html')


def plot3(request, *args, **kwargs):
    return render(request, 'plot3.html')


def plot4(request, *args, **kwargs):
    return render(request, 'plot4.html')


def authized_cap(request):
    url_data = request.GET['data']
    input_data = list(map(int, url_data.split(',')))

    input1, input2, input3, input4, input5 = [x for x in input_data]

    count1 = MaharashtraCompanyData.objects.filter(
        authorized_cap__gte=input1)\
        .filter(authorized_cap__lt=input2)\
        .count()
    count2 = MaharashtraCompanyData.objects.filter(
        authorized_cap__gte=input2)\
        .filter(authorized_cap__lt=input3)\
        .count()
    count3 = MaharashtraCompanyData.objects.filter(
        authorized_cap__gte=input3)\
        .filter(authorized_cap__lt=input4)\
        .count()
    count4 = MaharashtraCompanyData.objects.filter(
        authorized_cap__gte=input4)\
        .filter(authorized_cap__lt=input5)\
        .count()
    count5 = MaharashtraCompanyData.objects.filter(
        authorized_cap__gte=input5)\
        .count()

    bar1 = "< " + str(input2)
    bar2 = str(input2) + " - " + str(input3)
    bar3 = str(input3) + " - " + str(input4)
    bar4 = str(input4) + " - " + str(input5)
    bar5 = "> " + str(input5)
    plotdata = {
        bar1: count1,
        bar2: count2,
        bar3: count3,
        bar4: count4,
        bar5: count5,
    }
    return JsonResponse(plotdata)


def company_registration(request):
    url_data = request.GET['param']
    input_data = list(map(int, url_data.split(',')))

    start_year, end_year = [x for x in input_data]

    plotdata = {}

    for year in range(start_year, end_year+1):
        count = MaharashtraCompanyData.objects.filter(
            date_of_registration__year=year)\
                .count()
        plotdata[year] = count

    return JsonResponse(plotdata)


def top_10_pba(request):
    url_data = request.GET['param']
    year = int(url_data)

    data = MaharashtraCompanyData.objects.filter(
        date_of_registration__year=year)\
        .values('pba_as_per_cin')\
        .annotate(count=Count('pba_as_per_cin'))\
        .order_by('-count')[:10]

    plotdata = {}

    for pba in data:
        plotdata[pba['pba_as_per_cin']] = pba['count']

    return JsonResponse(plotdata)


def registration_count(request):
    url_data = request.GET['param']
    input_data = list(map(int, url_data.split(',')))

    pba_list = [
        "Agriculture and Allied Activities",
        "Business Services",
        "Community, personal & Social Services",
        "Construction",
        "Finance",
        "Insurance",
        "Manufacturing (Food stuffs)",
        "Trading",
        "Transport, storage and Communications",
        "Real Estate and Renting",
    ]

    selected_pba = []
    for val in input_data:
        selected_pba.append(pba_list[val])

    plotdata = {}

    data = MaharashtraCompanyData.objects.filter(
         date_of_registration__year__gte=2009)\
        .filter(date_of_registration__year__lte=2019)\
        .filter(pba_as_per_cin__in=selected_pba)\
        .values_list('pba_as_per_cin', 'date_of_registration__year')\
        .annotate(count=Count('pba_as_per_cin'))\
        .order_by('date_of_registration__year', 'pba_as_per_cin')

    for pba, year, count in data:
        plotdata[year] = plotdata.get(year, {})
        plotdata[year][pba] = count

    return JsonResponse(plotdata)
